from datetime import date, datetime, time, timezone

from nomnomdata.engine import Engine, Parameter, ParameterGroup, connections, parameters
from nomnomdata.engine.components import SharedConfig
from nomnomdata.engine.encoders import ModelEncoder


def test_parameter_serialization(snap, dummy_shared_config):
    pg = ParameterGroup(
        Parameter(
            type=dummy_shared_config,
            name="shared_config",
            display_name="Shared Config",
            description="",
        ),
        Parameter(
            connections.SSHHostConnection,
            name="ssh_connection",
            display_name="SSH Connection",
            description="Blah",
        ),
        Parameter(
            parameters.Code(max=512, min=5, dialect=parameters.CodeDialectType.JSON),
            name="code_type",
            display_name="Code Type",
            help_header_id="Some Heading",
            required=True,
            description="Description of parameter",
        ),
        Parameter(
            parameters.Boolean(),
            name="boolean_type",
            display_name="Boolean Type",
            help_md_path="./some-path",
            description="Description of parameter",
        ),
        Parameter(
            parameters.Int(min=0, max=100),
            name="int_type",
            display_name="Boolean Type",
            help_md_path="./some-path",
            description="Description of parameter",
        ),
        Parameter(
            parameters.String(min=0, max=1000),
            name="string_type",
            display_name="Boolean Type",
            help_md_path="./some-path",
            description="Description of parameter",
        ),
        Parameter(
            parameters.Text(min=0, max=1000),
            name="text_type",
            display_name="Boolean Type",
            help_md_path="./some-path",
            description="Description of parameter",
        ),
        Parameter(
            parameters.Nested(
                Parameter(
                    type=parameters.Int(), name="nested_int", display_name="Nested Int"
                ),
                Parameter(
                    type=parameters.String(),
                    name="nested_string",
                    display_name="Nested String",
                ),
            ),
            name="nested_type",
            many=True,
            display_name="Nested type",
        ),
        Parameter(
            parameters.DateTime(),
            default=datetime(2018, 1, 1, tzinfo=timezone.utc),
            name="datetime_type",
            display_name="Datetime",
        ),
        Parameter(
            parameters.Date(),
            default=date(2018, 1, 1),
            name="date_type",
            many=True,
            categories=["date", "test-category"],
            display_name="Date",
        ),
        Parameter(
            parameters.Time(),
            default=time(5, 1, 1, tzinfo=timezone.utc),
            name="time_type",
            display_name="Time",
        ),
    )

    engine = Engine(uuid="Something", alias="boop")

    @engine.action("something")
    @engine.parameter_group(pg)
    def test_action(*args):
        print(args)

    snap(engine, cls=ModelEncoder)


def test_shared_config_serialization(snap, dummy_shared_config):
    snap(dummy_shared_config, cls=ModelEncoder)


def test_connection_serialization(snap, dummy_connection):
    snap(dummy_connection, cls=ModelEncoder)


def test_nested_many(snap, dummy_shared_config_nested_many):
    snap(dummy_shared_config_nested_many, cls=ModelEncoder)
