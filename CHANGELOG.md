# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed

- Enum parameters now enforce uniqueness of `choices`

## [1.1.1] - 2020-12-22

### Added

- MSSQL Connection

## [1.1.0] - 2020-12-21

### Added

- S3 bucket SharedConfig

### Fixed

- Nested not rendering properly when `many` is passed
