from datetime import date, datetime, time, timezone

import pytest

from nomnomdata.engine import parameters
from nomnomdata.engine.components import Parameter, Rule
from nomnomdata.engine.encoders import ModelEncoder
from nomnomdata.engine.errors import LoadError, ValidationError


def test_time_type(snap):
    date_param = parameters.Time()
    snap(date_param, cls=ModelEncoder)

    assert date_param.dump(time(12, 0, tzinfo=timezone.utc)) == "12:00:00+00:00"
    assert date_param.load("06:04:02+00:00") == time(6, 4, 2, tzinfo=timezone.utc)


def test_date_type(snap):
    date_param = parameters.Date()
    snap(date_param, cls=ModelEncoder)
    assert date_param.dump(date(2018, 1, 1)) == "2018-01-01"
    assert date_param.load("2018-01-01") == date(2018, 1, 1)


def test_datetime_type(snap):
    date_param = parameters.DateTime()
    snap(date_param, cls=ModelEncoder)

    assert (
        date_param.dump(datetime(2018, 1, 1, 0, 0, 0, 1, tzinfo=timezone.utc))
        == "2018-01-01T00:00:00.000001+00:00"
    )
    assert date_param.load("2018-01-01T00:00:00.000001+00:00") == datetime(
        2018, 1, 1, 0, 0, 0, 1, tzinfo=timezone.utc
    )
    with pytest.raises(ValidationError):
        date_param.validate(date_param.load("2018-01-01T00:00:00.000001"))
    with pytest.raises(ValidationError):
        date_param.validate(datetime(2018, 1, 1, 0, 0, 0, 1))


def test_enum_type(snap):
    dedupe = parameters.Enum(["foo", "foo", "bar"])
    snap(dedupe, cls=ModelEncoder)
    enum_param = parameters.Enum(["foo", "bar"])
    snap(enum_param, cls=ModelEncoder)

    assert enum_param.dump("bar") == "bar"
    assert enum_param.validate("bar")

    assert enum_param.load("foo") == "foo"
    assert enum_param.validate("foo")

    with pytest.raises(ValidationError):
        enum_param.validate("barrington")
    with pytest.raises(ValidationError):
        enum_param.validate(enum_param.load(1))
    with pytest.raises(ValidationError):
        enum_param.validate(enum_param.load(["str", "str2"]))


def test_enum_list_type(snap):
    enum_param = parameters.EnumList(
        ["foo", "bar"],
        display_type=parameters.EnumDisplayType.checkbox_group,
    )
    snap(enum_param, cls=ModelEncoder)

    assert enum_param.dump(["bar"]) == ["bar"]
    assert enum_param.validate(["bar"])

    assert enum_param.load(["foo"]) == ["foo"]
    assert enum_param.validate(["foo"])

    assert enum_param.load(["foo", "bar"]) == ["foo", "bar"]
    assert enum_param.validate(["foo", "bar"])

    with pytest.raises(ValidationError):
        enum_param.validate(enum_param.load(1))
    with pytest.raises(ValidationError):
        enum_param.validate(enum_param.load(["barrington", "foo"]))
    with pytest.raises(ValidationError):
        enum_param.validate(enum_param.load("bar"))


def test_rule_type(dummy_shared_config, snap):
    params = Parameter(
        type=dummy_shared_config,
        name="subdomain",
        display_name="Subdomain",
        description="",
        required=True,
        show_on=Rule(name="test1", value=["test2"]),
    )
    snap(params, cls=ModelEncoder)


def test_bad_default():
    with pytest.raises(ValidationError):
        Parameter(
            type=parameters.EnumList(
                ["foo", "bar"],
                display_type=parameters.EnumDisplayType.checkbox_group,
            ),
            name="test",
            default=5,
        )


def test_shared_config_type(dummy_shared_config, snap):

    params = Parameter(
        type=dummy_shared_config,
        name="subdomain",
        display_name="Subdomain",
        description="",
        required=True,
    )
    snap(params, cls=ModelEncoder)

    deserialized = {
        "string_parameter": "junk",
        "integer_parameter": 2,
        "date_time_parameter": datetime(2018, 1, 1, 0, 0, 0, 1, tzinfo=timezone.utc),
        "nested_many_parameter": [{"nested_int_1": 5}, {"nested_int_1": 10}],
    }
    serialized = {
        "string_parameter": "junk",
        "integer_parameter": 2,
        "date_time_parameter": "2018-01-01T00:00:00.000001+00:00",
        "nested_many_parameter": [{"nested_int_1": 5}, {"nested_int_1": 10}],
    }
    assert params.load(serialized) == deserialized

    # We only handle loading the json to datetime not the other way around at this time.
    with pytest.raises(LoadError):
        params.load(deserialized)


def test_shared_config_type_many(dummy_shared_config_nested_many, snap):
    params = Parameter(
        type=dummy_shared_config_nested_many,
        name="subdomain",
        display_name="Subdomain",
        description="",
        required=True,
    )
    snap(params, cls=ModelEncoder)

    deserialized = {
        "shared_config_uuid": 1,
        "start_date": datetime(2018, 1, 1, 0, 0, 0, 1, tzinfo=timezone.utc),
        "column_parameters": [
            {"column_name": "junk", "column_type": "VARCHAR"},
            {"column_name": "junk2", "column_type": "VARCHAR"},
        ],
    }
    serialized = {
        "shared_config_uuid": 1,
        "start_date": "2018-01-01T00:00:00.000001+00:00",
        "column_parameters": [
            {"column_name": "junk", "column_type": "VARCHAR"},
            {"column_name": "junk2", "column_type": "VARCHAR"},
        ],
    }
    assert params.load(serialized) == deserialized

    # We only handle loading the json to datetime not the other way around at this time.
    with pytest.raises(LoadError):
        params.load(deserialized)


def test_password_type(snap):

    param = Parameter(
        type=parameters.Password(),
        name="password_type",
        display_name="Password",
        description="",
        required=True,
    )

    snap(param, cls=ModelEncoder)
