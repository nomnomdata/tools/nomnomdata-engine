def convert_to_nominode_params(test_parameters, connections):
    params = test_parameters.copy()
    params["config"] = {str(i): v for i, v in enumerate(connections.values())}
    params["action_name"] = "test_action"
    params["alias"] = "placeholder"
    for i, connection in enumerate(connections):
        params[connection] = {"connection_uuid": str(i)}
    return params
