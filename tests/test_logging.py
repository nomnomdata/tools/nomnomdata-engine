import logging

from nomnomdata.engine import Engine, ParameterGroup
from nomnomdata.engine.testing import EngineContextMock

from .util import convert_to_nominode_params


def test_connection_masking(
    dummy_engine: Engine,
    dummy_parameter_group: ParameterGroup,
    test_parameters,
    test_connections,
    snap,
):
    @dummy_engine.action("Some Action")
    @dummy_engine.parameter_group(dummy_parameter_group)
    def test_action(*args):
        logger = logging.getLogger("testlogger")
        logger.error("SECRET unmasked 1111")

    with EngineContextMock(
        engine=dummy_engine,
        task_parameters=convert_to_nominode_params(test_parameters, test_connections),
    ) as em:
        dummy_engine._run()
        calls = em.nominode_ctx.calls
        assert len(calls) == 3
        snap(
            calls[2][1],
            exclude=[
                "created",
                "msecs",
                "relativeCreated",
                "thread",
                "process",
                "pathname",
                "lineno",
            ],
        )


def test_no_connections(
    dummy_engine: Engine,
    parameter_group_no_connections: ParameterGroup,
    snap,
):
    @dummy_engine.action("Some Action")
    @dummy_engine.parameter_group(parameter_group_no_connections)
    def test_action(*args):
        logger = logging.getLogger("testlogger")
        logger.error("SECRET unmasked 1111")

    with EngineContextMock(
        engine=dummy_engine,
        task_parameters=convert_to_nominode_params({"int_parameter": 5}, {}),
    ) as em:
        dummy_engine._run()
        calls = em.nominode_ctx.calls
        assert len(calls) == 3
        snap(
            calls[2][1],
            exclude=[
                "created",
                "msecs",
                "relativeCreated",
                "thread",
                "process",
                "pathname",
                "lineno",
            ],
        )
