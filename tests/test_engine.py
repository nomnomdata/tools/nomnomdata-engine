import pytest

from nomnomdata.engine import Engine, ParameterGroup
from nomnomdata.engine.errors import LoadError, MissingParameters, ValidationError
from nomnomdata.engine.testing import NominodeContextMock

from .util import convert_to_nominode_params


class SpecialException(Exception):
    pass


class does_not_raise:
    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        pass


@pytest.mark.parametrize(
    "task_params,connections,expectation",
    [
        (
            # all parameters present
            {
                "int_parameter": 100,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": "sss",
            },
            {
                "dummy_connection": {"connection_int": 1, "connection_string": "test"},
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            does_not_raise(),
        ),
        (
            # non required parameters None
            {
                "int_parameter": None,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": "sss",
            },
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            does_not_raise(),
        ),
        (
            # non required parameters missing
            {
                "required_string_parameter": "sss",
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
            },
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            does_not_raise(),
        ),
        (
            # required parameter None
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": None,
            },
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            pytest.raises(MissingParameters),
        ),
        (
            # missing required connection
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": "sss",
            },
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                }
            },
            pytest.raises(MissingParameters),
        ),
        (
            # required connection None
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": "sss",
            },
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": None,
            },
            pytest.raises(MissingParameters),
        ),
        (
            # parameter fails validation
            {
                "int_parameter": "sssss",
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": None,
            },
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            pytest.raises(ValidationError),
        ),
        (
            # required connection parameter None
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": None,
            },
            {
                "dummy_connection": {"connection_int": None},
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            pytest.raises(MissingParameters),
        ),
        (
            # connection parameter fails validation
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": None,
            },
            {
                "dummy_connection": {"connection_int": "ssss"},
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            pytest.raises(ValidationError),
        ),
        (
            # many parameter fails loading
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "NOT A DATETIME",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": None,
            },
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            pytest.raises(LoadError),
        ),
        (
            # many parameter fails validation
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 300],
                "required_string_parameter": None,
            },
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            pytest.raises(ValidationError),
        ),
        (
            # Testing shared config
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": "sss",
                "dummy_shared_config": {
                    "string_parameter": "junk",
                    "integer_parameter": 2,
                    "date_time_parameter": "2018-01-01T00:00:00.000001+00:00",
                },
            },
            # Connections parameters separated because they need to be separated out
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            does_not_raise(),
        ),
        (
            # Testing shared config Validation Error
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": "sss",
                "dummy_shared_config": {
                    "string_parameter": "junk",
                    "integer_parameter": 2,
                    "date_time_parameter": "2018-01-01T00:00:00.000001",
                },
            },
            # Connections parameters separated because they need to be separated out
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            pytest.raises(ValidationError),
        ),
        (
            # Testing shared config Load Error
            {
                "int_parameter": 5,
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": "sss",
                "dummy_shared_config": {
                    "string_parameter": "junk",
                    "integer_parameter": 2,
                    "date_time_parameter": "BD(B3;asd;ksdf'as;",
                },
            },
            # Connections parameters separated because they need to be separated out
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            pytest.raises(LoadError),
        ),
        (
            # Testing junk params
            {
                "int_parameter": 5,
                "junk": "some junk",
                "datetime_many_parameter": [
                    "2018-01-01T00:00:00.000000+00:00",
                    "2019-01-01T00:00:00.000000+00:00",
                ],
                "int_many_parameter": [1, 2, 3],
                "required_string_parameter": "sss",
                "dummy_shared_config": {
                    "junk": "delete",
                    "string_parameter": "junk",
                    "integer_parameter": 2,
                    "date_time_parameter": "2018-01-01T00:00:00.000001+00:00",
                },
            },
            # Connections parameters separated because they need to be separated out
            {
                "dummy_connection": {
                    "connection_int": 1,
                    "connection_string": "test",
                    "junk": "delete",
                },
                "dummy_connection_required": {
                    "connection_int": 1,
                    "connection_string": "test",
                },
            },
            does_not_raise(),
        ),
    ],
    [],
)
def test_engine_run(
    task_params,
    connections,
    expectation,
    dummy_engine: Engine,
    dummy_parameter_group: ParameterGroup,
    snap,
):
    @dummy_engine.action("Some Action")
    @dummy_engine.parameter_group(dummy_parameter_group)
    def test_action(*args):
        dummy_engine.update_parameter("int_parameter", 5)
        dummy_engine.update_progress(90)
        snap(args)
        return "result"

    with expectation, NominodeContextMock(
        task_parameters=convert_to_nominode_params(task_params, connections)
    ) as nm:
        dummy_engine._run()
        snap([body for url, body in nm.calls if url == "/task/TASK_UUID/parameters"][-1])
        snap([body for url, body in nm.calls if url == "/execution/update/TEST_UUID"][-1])

    direct_params = task_params.copy()
    direct_params.update(connections)
    with expectation:
        api_calls, result = test_action(**direct_params)
        assert result == "result"
        snap([body for url, body in api_calls if url == "/task/TASK_UUID/parameters"][-1])
        snap(
            [body for url, body in api_calls if url == "/execution/update/TEST_UUID"][-1]
        )


def test_engine_exception(
    dummy_engine: Engine,
    dummy_parameter_group: ParameterGroup,
    test_parameters,
    test_connections,
):
    @dummy_engine.action("Some Action")
    @dummy_engine.parameter_group(dummy_parameter_group)
    def test_action(*args):
        dummy_engine.update_parameter("int_parameter", 5)
        raise SpecialException()

    direct_params = test_parameters.copy()
    direct_params.update(test_connections)
    with pytest.raises(SpecialException):
        test_action(**direct_params)

    with NominodeContextMock(
        task_parameters=convert_to_nominode_params(test_parameters, test_connections)
    ) as nm:
        with pytest.raises(SpecialException):
            dummy_engine._run()
        body = [body for url, body in nm.calls if url == "/task/TASK_UUID/parameters"][-1]
        assert body == {
            "int_parameter": 5,
        }
