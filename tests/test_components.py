import pytest

from nomnomdata.engine import Parameter, parameters


def test_alias_protected():
    with pytest.raises(ValueError):
        Parameter(type=parameters.String(), name="alias")
