import inspect
import json
from pathlib import Path

import pytest
from genson import SchemaBuilder

from nomnomdata.engine import Engine, parameters
from nomnomdata.engine.components import (
    Connection,
    Parameter,
    ParameterGroup,
    SharedConfig,
)


def drop_keys(drop_key, obj):
    if isinstance(obj, dict):
        obj = {k: v for k, v in obj.items() if k != drop_key}
        for key in obj:
            obj[key] = drop_keys(drop_key, obj[key])
    elif isinstance(obj, list):
        obj = [drop_keys(drop_key, o) for o in obj]
    return obj


SNAPSHOTS_USED = set()


@pytest.fixture
def snap(snapshot, request):
    snap.counter = 0

    def match(obj, exclude=None, cls=None, default=None):
        if not cls and not default:

            def default(o):
                return str(o)

        if exclude:
            if isinstance(exclude, list):
                for ex in exclude:
                    obj = drop_keys(ex, obj)
            else:
                obj = drop_keys(exclude, obj)
        frameinfo = inspect.stack()[1]
        folder = Path(frameinfo.filename).parent
        snapshot.snapshot_dir = folder / Path("snapshots")
        snapshot_file = f"{request.node.listnames()[-1]}_{snap.counter}.json"
        snapshot.assert_match(
            json.dumps(obj, indent=4, cls=cls, default=default), snapshot_file
        )
        snap.counter += 1
        SNAPSHOTS_USED.add(snapshot_file)

    return match


SCHEMA_SNAPSHOTS_USED = set()


@pytest.fixture
def snap_schema(snapshot):
    snap_schema.counter = 0

    def match(obj):
        frameinfo = inspect.stack()[1]
        folder = Path(frameinfo.filename).parent
        snapshot.snapshot_dir = folder / Path("snapshots") / Path("schema")
        builder = SchemaBuilder()
        builder.add_object(obj)
        snapshot_file = f"{frameinfo.function}_{snap_schema.counter}.json"
        snapshot.assert_match(json.dumps(builder.to_schema(), indent=4), snapshot_file)
        snap_schema.counter += 1
        SCHEMA_SNAPSHOTS_USED.add(snapshot_file)

    return match


@pytest.fixture
def dummy_connection() -> Connection:
    return Connection(
        connection_type_uuid="DUMMY-CONNECTION",
        description="Description",
        alias="Alias",
        categories=["test", "test1"],
        parameter_groups=[
            ParameterGroup(
                Parameter(
                    type=parameters.Int(),
                    name="connection_int",
                    display_name="Dummy Param 1",
                    description="Dummy Description 1",
                    required=True,
                ),
                Parameter(
                    type=parameters.String(),
                    name="connection_string",
                    display_name="Dummy Param 2",
                    description="Dummy Description 2",
                    required=True,
                ),
            )
        ],
    )


@pytest.fixture
def dummy_int_parameter() -> Parameter:
    return Parameter(
        parameters.Int(),
        name="int_parameter",
        display_name="Int Type",
        help_md_path="./some-path",
        description="Description of parameter",
    )


@pytest.fixture
def parameter_group_no_connections(dummy_int_parameter: Parameter) -> ParameterGroup:
    return ParameterGroup(
        dummy_int_parameter,
        name="default-pg-name",
        display_name="Default PG Name",
        description="Default PG description",
    )


@pytest.fixture
def dummy_parameter_group(
    dummy_connection: Connection,
    dummy_int_parameter: Parameter,
    dummy_shared_config: SharedConfig,
) -> ParameterGroup:
    return ParameterGroup(
        Parameter(
            dummy_shared_config,
            name="dummy_shared_config",
            display_name="Dummy Shared Config",
            description="Blah",
        ),
        Parameter(
            dummy_connection,
            name="dummy_connection",
            display_name="Dummy Connection",
            description="Blah",
        ),
        Parameter(
            dummy_connection,
            name="dummy_connection_required",
            display_name="Dummy Connection",
            description="Blah",
            required=True,
        ),
        Parameter(
            parameters.String(),
            name="required_string_parameter",
            display_name="String Type",
            help_md_path="./some-path",
            description="Description of parameter",
            required=True,
        ),
        Parameter(
            parameters.DateTime(),
            name="datetime_many_parameter",
            display_name="Datetime Many",
            many=True,
        ),
        Parameter(
            parameters.Int(max=5),
            name="int_many_parameter",
            display_name="Datetime Many",
            many=True,
        ),
        dummy_int_parameter,
        name="default-pg-name",
        display_name="Default PG Name",
        description="Default PG description",
    )


@pytest.fixture
def dummy_engine() -> Engine:
    return Engine(uuid="Something", alias="boop")


@pytest.fixture
def dummy_action(dummy_engine: Engine, dummy_parameter_group: ParameterGroup) -> callable:
    @dummy_engine.action("something")
    @dummy_engine.parameter_group(dummy_parameter_group)
    def test_action(*args):
        print(args)

    return test_action


@pytest.fixture
def test_parameters():
    return {
        "int_parameter": 100,
        "datetime_many_parameter": [
            "2018-01-01T00:00:00.000000+00:00",
            "2019-01-01T00:00:00.000000+00:00",
        ],
        "int_many_parameter": [1, 2, 3],
        "required_string_parameter": "sss",
    }


@pytest.fixture
def test_connections():
    return {
        "dummy_connection": {"connection_int": 1, "connection_string": "SECRET"},
        "dummy_connection_required": {"connection_int": 1, "connection_string": "SECRET"},
    }


@pytest.fixture
def dummy_shared_config():
    return SharedConfig(
        shared_config_type_uuid="FAKE_SHARED_CONFIG",
        alias="Dummy SharedConfig alias",
        description="Dummy SharedConfig description",
        categories=["Dummy"],
        parameter_groups=[
            ParameterGroup(
                Parameter(
                    parameters.String(),
                    name="string_parameter",
                    display_name="String Parameter",
                    description="String to parse and write to the Task Execution Log.",
                ),
                Parameter(
                    parameters.Int(max=500000),
                    display_name="Integer Parameter",
                    name="integer_parameter",
                    description="Integer to parse and write to the Task Execution Log",
                ),
                Parameter(
                    parameters.DateTime(),
                    display_name="DateTime Parameter",
                    name="date_time_parameter",
                    description="Integer to parse and write to the Task Execution Log",
                ),
                Parameter(
                    parameters.Nested(
                        Parameter(
                            parameters.Int(),
                            name="nested_int_1",
                            display_name="Nested Int 1",
                        )
                    ),
                    many=True,
                    display_name="Nested Parameter",
                    name="nested_many_parameter",
                    description="Nested parameter in a shared config",
                ),
            )
        ],
    )


@pytest.fixture
def dummy_shared_config_nested_many():
    return SharedConfig(
        shared_config_type_uuid="S3BTK-DB3MD",
        description="Me",
        alias="S3 File to Relational Database",
        categories=["S3"],
        parameter_groups=[
            ParameterGroup(
                Parameter(
                    name="start_date",
                    display_name="Start Date",
                    description="Specify",
                    type=parameters.DateTime(),
                    required=False,
                ),
                name="key_parameters",
                display_name="Key Parameters",
                description="Columns ",
            ),
            # MANY TYPES ARE NOT PART OF PARAMETER GROUPS FOR SHARED OBJECTS
            Parameter(
                name="column_parameters",
                display_name="Column Parameters",
                description="Details about each column within the relational database.",
                required=True,
                many=True,
                type=parameters.Nested(
                    Parameter(
                        name="column_name",
                        display_name="Column Name",
                        description="Specify the name of the column.",
                        type=parameters.String(max=128),
                        required=True,
                    ),
                    Parameter(
                        name="column_type",
                        display_name="Column Data Type",
                        description="Sel",
                        type=parameters.Enum(
                            choices=[
                                "VARCHAR",
                            ]
                        ),
                        required=True,
                    ),
                ),
            ),
        ],
    )
